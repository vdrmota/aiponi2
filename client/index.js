const postchain = require('postchain-client')
const secp256k1 = require('secp256k1');
const randomBytes = require('crypto').randomBytes;
const csv = require('csvtojson')

const csvFilePath='logs.csv'

function makeKeyPair () {
    let privKey;

    do {
       privKey = randomBytes(32);
    } while (!secp256k1.privateKeyVerify(privKey));

    const pubKey = secp256k1.publicKeyCreate(privKey);
    return {pubKey, privKey};
}

// Create some dummy keys
const aliceKeyPair = makeKeyPair();

const blockchainRID = Buffer.from("5d0502f38c4610e702ee85754f2e5a94c1907c58fc793f3920b2b663ce623fe3", "hex");

const restClient = postchain.restClient.createRestClient("http://localhost:7740", 5);
const gtxClient = postchain.gtxClient.createClient(
    restClient,
    blockchainRID,
    ["add_payment"]
);

function waitConfirmation(txid) {
        return new Promise( (resolve, reject) => {
            setTimeout(() => {
                console.log("Polling ", txid.toString('hex'));
                restClient.status(txid, (err, res) => {
                    console.log("Got status response:", err, res);
                    if (err) reject(err);
                    else {
                        const {status} = res;
                        switch (status) {
                            case "confirmed": resolve(txid); break;
                            case "rejected": reject(Error("Message was rejected")); break;
                            case "unknown": reject(Error("Server lost our message")); break;
                            case "waiting":
                                waitConfirmation(txid).then(resolve, reject);
                                break;
                            default:
                            console.log(status);
                            reject(Error("got unexpected response from server"));
                    }
                }
            });
        }, 511);
    });
}


function sendAndWaitConfirmation(rq) {
    const buffer = rq.getBufferToSign();
    const txRID = postchain.util.sha256(buffer);
    return (new Promise( (resolve, reject) => {
        rq.send( (error) => {
            if (error) reject(error);
            else resolve(txRID);
        });
    })).then( (txRID) => {
        return waitConfirmation(txRID);
    });
}

function standardResultHandler(resolve, reject) {
    const resultHandler = (error, result) => {
        if (error) {
            reject(error);
        }
        if (result.hits === 0) {
            // poll every 2 sec
            setTimeout(gtx.query(queryObject, resultHandler), 2000);
        }
        resolve(result)
    }
    return resultHandler;
}

function addPayment(privKey, fromAddress, payment, record_date, identification, amount) {
    const rq = gtxClient.newRequest([fromAddress]);
    rq.add_payment(parseInt(payment), parseInt(record_date), identification, parseInt(amount));
    rq.sign(privKey, fromAddress);
    return sendAndWaitConfirmation(rq);
}


csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{
    jsonObj.forEach(function (el){
             addPayment(aliceKeyPair.privKey, aliceKeyPair.pubKey, el.payment, Date.now(), el.id, el.amount)
        })

})

