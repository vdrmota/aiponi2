# Running AionPostchainNigeria

## Prerequisites
1.  **Java 8**
    * Postchain runs on [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
    * Make sure `java -version` displays Java 8 as your version.
2. **PostgreSQL**
    * Install [PostgreSQL](https://www.postgresql.org/download/).
2. **NodeJS**
    * Install [NodeJS](https://nodejs.org/en/download/).
## Set up

Set up PostgreSQL for Postchain:

On MacOS:
```
sudo -u _postgres -i
```
On Linux:
```
sudo -u postgres -i
```
Then:
```
createdb postchain
psql -c "create role postchain LOGIN ENCRYPTED PASSWORD 'postchain'" -d postchain
psql -c "grant ALL ON DATABASE postchain TO postchain" -d postchain
sudo exit
```

## Run Postchain Node

Run the Postchain node:

`./postchain.sh -j aionpostchainnigeria-1.0-SNAPSHOT.jar -i 0 -c single.properties`

There might be some warnings at the start, ignore those.

## Send Transactions

To send transactions from example phone payment logs stored in client/logs.csv to the Postchain node:
```
cd client
node index
```
Make sure your postchain node is running when you send the transactions.

## Explore Postchain Data

To display the PostgreSQL tables associated with the Postchain node:
```
psql "user=postchain password=postchain dbname=postchain host=127.0.0.1"
SET search_path TO etk;
\dt
```

To show Postchain block data (encoded with ASN.1):
```
SELECT * FROM blocks;
```

To show log data inserted into PostgreSQL RDB:
```
SELECT * FROM phone_payments;
```

